use std::convert::{TryInto, TryFrom};
use std::io::Write;
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;
use std::time::Instant;

use bitcoin::{BlockHash, Network};
use bitcoin::consensus::Encodable;
use bitcoin::hashes::Hash;
use serde::{Serialize, Deserialize};

use crate::tree::{PartialMerkleTree, UtxoMerkleNode, merkle_root_from_level0_async};
use crate::utxo::{Utxos, UtxosDelta};
use std::fs::File;
use std::collections::HashMap;
use rocksdb::{DB, Options, ColumnFamilyDescriptor, WriteBatch, IteratorMode, BoundColumnFamily};
use tokio::runtime::Handle;

const CURRENT_EPOCH_VAR: &str = "current_epoch_end";
const ROCKSDB_MEMORY: usize = 1024 * 1024 * 1024;

struct UtxoStorageState {
	// The hash of the last block of the last complete epoch (the "current" epoch)
	previous_epoch_hash: Option<BlockHash>,
	// Returns true if the epoch root was computed (not fast-syncing)
	epoch_task: Option<JoinHandle<bool>>,
}

pub struct UtxoSledStorage {
	path: String,
	#[allow(unused)]
	db: Arc<DB>,
	state: Arc<Mutex<UtxoStorageState>>,
	epoch_length: u32,
	fast_sync_height: u32,
}

impl Clone for UtxoSledStorage {
	fn clone(&self) -> Self {
		UtxoSledStorage {
			path: self.path.clone(),
			db: Arc::clone(&self.db),
			state: Arc::clone(&self.state),
			epoch_length: self.epoch_length,
			fast_sync_height: self.fast_sync_height
		}
	}
}

#[derive(Serialize, Deserialize, Debug)]
struct BlockCommit {
	adds_root: UtxoMerkleNode,
	removes_root: UtxoMerkleNode,
	delta_to_epoch: BlockHash,
}

impl Into<Vec<u8>> for BlockCommit {
	fn into(self) -> Vec<u8> {
		serde_cbor::to_vec(&self).unwrap()
	}
}

impl TryFrom<Vec<u8>> for BlockCommit {
	type Error = ();

	fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
		serde_cbor::from_slice(&value).map_err(|_| ())
	}
}

#[derive(Serialize, Deserialize, Debug)]
struct EpochCommit {
	root: Option<UtxoMerkleNode>,
	previous_epoch: Option<BlockHash>,
}

impl Into<Vec<u8>> for EpochCommit {
	fn into(self) -> Vec<u8> {
		serde_cbor::to_vec(&self).unwrap()
	}
}

impl TryFrom<Vec<u8>> for EpochCommit {
	type Error = ();

	fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
		serde_cbor::from_slice(&value).map_err(|_| ())
	}
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct HeightHash {
	height: u32,
	block_hash: BlockHash,
}

impl HeightHash {
	fn serialize(&self) -> [u8; 36] {
		let mut buf = [0u8; 36];
		buf[0..4].copy_from_slice(&self.height.to_be_bytes());
		buf[4..].copy_from_slice(&self.block_hash);
		buf
	}

	fn deserialize(buf: &[u8]) -> Result<Self, ()> {
		let height = u32::from_be_bytes(buf[0..4].try_into()
			.map_err(|_| ())?);
		let block_hash = BlockHash::from_slice(&buf[4..])
			.map_err(|_| ())?;
		Ok(HeightHash {
			height,
			block_hash
		})
	}
}

impl Into<Vec<u8>> for HeightHash {
	fn into(self) -> Vec<u8> {
		serde_cbor::to_vec(&self).unwrap()
	}
}

impl TryFrom<Vec<u8>> for HeightHash {
	type Error = ();

	fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
		serde_cbor::from_slice(&value).map_err(|_| ())
	}
}

fn compute_merkle_root(nodes: &Vec<UtxoMerkleNode>) -> UtxoMerkleNode {
	if nodes.is_empty() {
		return UtxoMerkleNode::default();
	}
	let matches: Vec<bool> = (0..nodes.len()).map(|_| false).collect();
	let pmt = PartialMerkleTree::from_utxos(&nodes, &matches);
	let root = pmt.extract_matches(&mut Vec::new(), &mut Vec::new()).unwrap();
	root
}

fn compute_delta_merkle_root(utxos: &Utxos) -> UtxoMerkleNode {
	let mut utxos = utxos.to_vec();
	utxos.sort();
	let mut utxo_nodes = Vec::new();
	for outpoint in utxos.iter() {
		let mut engine = UtxoMerkleNode::engine();
		outpoint.consensus_encode(&mut engine).unwrap();
		utxo_nodes.push(UtxoMerkleNode::from_engine(engine))
	}
	compute_merkle_root(&utxo_nodes)
}

pub trait Storage {
	fn connect_block(&self, new_height: u32, epoch_end_height: BlockHash, delta_to_previous_epoch: &UtxosDelta);
	fn next_epoch(&self, new_height: u32, epoch_end_height: BlockHash, delta_to_current_epoch: &UtxosDelta);
}

impl UtxoSledStorage {
	pub async fn new(network: Network, epoch_length: u32, fast_sync_height: u32) -> Self {
		if fast_sync_height > 0 {
			log::info!("fast sync to {}", fast_sync_height);
		}
		let path = format!("storage-{}", network.to_string());

		let mut db_opts = Options::default();
		db_opts.create_missing_column_families(true);
		db_opts.create_if_missing(true);

		let mut cf_opts = Options::default();
		cf_opts.set_max_write_buffer_number(16);

		let mut utxos_cf_opts = Options::default();
		utxos_cf_opts.optimize_level_style_compaction(ROCKSDB_MEMORY);

		let cf_descriptors = vec![
			ColumnFamilyDescriptor::new("utxos", utxos_cf_opts.clone()),
			ColumnFamilyDescriptor::new("vars", cf_opts.clone()),
			ColumnFamilyDescriptor::new("epochs", cf_opts.clone()),
			ColumnFamilyDescriptor::new("blocks", cf_opts.clone()),
		];
		let db = Arc::new(DB::open_cf_descriptors(&db_opts, &path, cf_descriptors).unwrap());

		let vars_cf = db.cf_handle("vars").unwrap();
		let epochs_cf = db.cf_handle("epochs").unwrap();

		log::info!("opened db");
		// The Builder will re-accumulate the delta from the previous epoch, and we will
		// start being notified of blocks of the current epoch.
		let state = UtxoStorageState { previous_epoch_hash: None, epoch_task: None };
		let storage = UtxoSledStorage {
			path,
			db: Arc::clone(&db),
			state: Arc::new(Mutex::new(state)),
			epoch_length,
			fast_sync_height
		};

		let current_epoch: Option<HeightHash> = db.get_cf(vars_cf, CURRENT_EPOCH_VAR).unwrap()
			.map(|v| v.try_into().unwrap());
		let previous_epoch_hash = if let Some(hh) = current_epoch {
			if hh.height >= fast_sync_height {
				let commit: EpochCommit = db.get_cf(epochs_cf, hh.serialize()).unwrap().unwrap()
					.try_into().unwrap();
				log::info!("storage current epoch {:?} {:?}", hh, commit);
				let previous_epoch = commit.previous_epoch;
				if commit.root.is_none() {
					storage.roll_forward_epoch_commit(hh, commit).await;
				}
				previous_epoch
			} else { None }
		} else { None };
		storage.state.lock().unwrap().previous_epoch_hash = previous_epoch_hash;
		storage
	}

	fn utxos_cf(&self) -> BoundColumnFamily {
		self.db.cf_handle("utxos").unwrap()
	}

	fn vars_cf(&self) -> BoundColumnFamily {
		self.db.cf_handle("vars").unwrap()
	}

	fn blocks_cf(&self) -> BoundColumnFamily {
		self.db.cf_handle("blocks").unwrap()
	}

	fn epochs_cf(&self) -> BoundColumnFamily {
		self.db.cf_handle("epochs").unwrap()
	}

	pub fn current_epoch_height(&self) -> Option<u32> {
		let current_epoch: Option<HeightHash> = self.db.get_cf(self.vars_cf(),CURRENT_EPOCH_VAR).unwrap()
			.map(|ivec| ivec.try_into().unwrap());
		if let Some(current_epoch) = current_epoch {
			Some(current_epoch.height)
		} else {
			None
		}
	}

	pub fn dump(&self) {
		let epochs = {
			let mut epochs = HashMap::new();
			let mut epoch_commitments_out = File::create(format!("{}/epochs.csv", self.path)).unwrap();
			let mut last_height = None;
			for (k,v) in self.db.iterator_cf(self.epochs_cf(), IteratorMode::Start) {
				let height_hash = HeightHash::deserialize(&k).unwrap();
				let commit = EpochCommit::try_from(v.to_vec()).unwrap();
				write!(&mut epoch_commitments_out, "{:?} {:?}\n", height_hash, commit).unwrap();
				if let Some(last) = last_height {
					if last + self.epoch_length != height_hash.height {
						println!("missing epochs last {} current {:?} diff {}", last, height_hash, height_hash.height - last)
					}
				}
				last_height = Some(height_hash.height);
				epochs.insert(height_hash.height, height_hash.block_hash);
			}
			epochs
		};

		{
			let mut block_commitments_out = File::create(format!("{}/blocks.csv", self.path)).unwrap();
			for (k, v) in self.db.iterator_cf(self.blocks_cf(), IteratorMode::Start) {
				let height_hash = HeightHash::deserialize(&k).unwrap();
				let commit: BlockCommit = v.to_vec().try_into().unwrap();
				let epoch_height = (height_hash.height - height_hash.height % self.epoch_length) - self.epoch_length - 1;
				if Some(&commit.delta_to_epoch) != epochs.get(&epoch_height) {
					println!("epoch not found at {} epoch {}", height_hash.height, epoch_height);
				}
				write!(&mut block_commitments_out, "{:?} {:?}\n", height_hash, commit).unwrap();
			}
		}
	}
}

impl Storage for UtxoSledStorage {
	fn connect_block(&self, height: u32, block_hash: BlockHash, delta_to_previous_epoch: &UtxosDelta) {
		let delta = delta_to_previous_epoch.minimize();
		let previous_epoch_hash_opt = self.state.lock().unwrap().previous_epoch_hash;
		if let Some(previous_epoch_hash) = previous_epoch_hash_opt {
			let now = Instant::now();
			let adds_root = compute_delta_merkle_root(&delta.adds);
			let removes_root = compute_delta_merkle_root(&delta.removes);
			let block_commit = BlockCommit {
				adds_root,
				removes_root,
				delta_to_epoch: BlockHash::from_slice(&previous_epoch_hash).unwrap(),
			};
			let height_hash = HeightHash { height, block_hash };
			let commit_vec: Vec<u8> = block_commit.into();
			self.db.put_cf(self.blocks_cf(), height_hash.serialize(), commit_vec).unwrap();
			log::info!("computed block commitment height {} in {} ms", height, now.elapsed().as_millis());
		}
	}

	fn next_epoch(&self, height: u32, block_hash: BlockHash, delta_to_current_epoch: &UtxosDelta) {
		let delta = delta_to_current_epoch.minimize();

		// if this was the last block of the epoch, do the epoch shift
		assert_eq!(height % self.epoch_length, self.epoch_length - 1);
		let now = Instant::now();
		let mut lock = self.state.lock().unwrap();
		let mut epoch_commit_computed = false;
		if let Some(handle) = lock.epoch_task.take() {
			epoch_commit_computed |= handle.join().unwrap();
			log::info!("waited for join {} ms", now.elapsed().as_millis());
		}

		// We need the previous epoch hash when no epoch task is running, or we'll get a race condition.
		// The task that just finished was kicked off one epoch ago, and it processed the epoch before that.
		// So CURRENT_EPOCH block hash from storage is actually the previous epoch block hash.
		let previous_epoch: Option<HeightHash> = self.db.get_cf(self.vars_cf(), CURRENT_EPOCH_VAR).unwrap()
			.map(|ivec| ivec.try_into().unwrap());
		if epoch_commit_computed {
			lock.previous_epoch_hash = previous_epoch.map(|hh| hh.block_hash);
		}

		let self_ = self.clone();
		let handle = Handle::current();
		let epoch_task = thread::spawn(
			move || {
				handle.block_on(self_.do_end_epoch(height, block_hash, delta))
			});
		lock.epoch_task = Some(epoch_task);
	}
}


impl UtxoSledStorage {
	async fn roll_forward_epoch_commit(&self, height_hash: HeightHash, commit: EpochCommit) {
		log::warn!("rolling forward crashed epoch commit at {}", height_hash.height);
		let root = Some(self.compute_root(height_hash.height).await);
		let epoch_commit = EpochCommit { root, previous_epoch: commit.previous_epoch };
		let vec: Vec<u8> = epoch_commit.into();
		self.db.put_cf(self.epochs_cf(), height_hash.serialize().to_vec(), vec).unwrap();
	}

	async fn do_end_epoch(&self, epoch_end_height: u32, block_hash: BlockHash, delta: UtxosDelta) -> bool {
		let now = Instant::now();
		log::info!("store epoch {} delta +{} -{}", epoch_end_height, delta.adds.len(), delta.removes.len());

		let height_hash = HeightHash { height: epoch_end_height, block_hash };

		let utxos = self.utxos_cf();

		// Update global UTXO set.
		let mut batch = WriteBatch::default();
		for add in delta.adds.iter() {
			let mut buf = Vec::new();
			add.consensus_encode(&mut buf).unwrap();
			batch.put_cf(utxos, buf, &[]);
		}
		for remove in delta.removes.iter() {
			let mut buf = Vec::new();
			remove.consensus_encode(&mut buf).unwrap();
			batch.delete_cf(utxos, buf);
		}
		let previous_epoch: Option<HeightHash> = self.db.get_cf(self.vars_cf(), CURRENT_EPOCH_VAR).unwrap().map(|e| e.try_into().unwrap());
		if let Some(hh) = previous_epoch.clone() {
			assert_eq!(epoch_end_height, hh.height + self.epoch_length);
		}
		let previous_epoch_hash = previous_epoch.map(|hh| hh.block_hash);
		let current_epoch = HeightHash { height: epoch_end_height, block_hash };
		let current_epoch_vec: Vec<u8> = current_epoch.into();
		batch.put_cf(self.vars_cf(), CURRENT_EPOCH_VAR, current_epoch_vec);
		// Temporarily store the commitment as None, we'll overwrite it below.
		// This preserves the invariant that CURRENT_EPOCH_VAR always points to an epoch commitment.
		// The two also act as a reminder to rollback the epoch if needed on reorg.
		let epoch_commit = EpochCommit { root: None, previous_epoch: previous_epoch_hash };
		let epoch_commit_vec : Vec<u8> = epoch_commit.into();
		batch.put_cf(self.epochs_cf(), height_hash.serialize().to_vec(), epoch_commit_vec);
		self.db.write(batch).unwrap();

		log::info!("stored epoch {} in {} ms", epoch_end_height, now.elapsed().as_millis());
		// FIXME handle crashing here

		let committed = if epoch_end_height >= self.fast_sync_height {
			let root = Some(self.compute_root(epoch_end_height).await);
			let epoch_commit = EpochCommit { root, previous_epoch: previous_epoch_hash };
			let vec: Vec<u8> = epoch_commit.into();
			self.db.put_cf(self.epochs_cf(), height_hash.serialize().to_vec(), vec).unwrap();
			true
		} else {
			false
		};

		committed
	}

	async fn compute_root(&self, epoch_end_height: u32) -> UtxoMerkleNode {
		let iter = self.db.iterator_cf(self.utxos_cf(), IteratorMode::Start)
			.map(|(k, _v)| k);
		let now = Instant::now();
		let root = merkle_root_from_level0_async(10, iter).await;
		log::info!("done epoch {} in {} ms, root {}", epoch_end_height, now.elapsed().as_millis(), root);
		root
	}
}

#[cfg(test)]
#[allow(unused)]
mod tests {
	extern crate rand;
	extern crate rocksdb;

	use std::time::{Instant, Duration};

	use bitcoin::hashes::Hash;

	use crate::tree::{PartialMerkleTree, UtxoMerkleNode};
	use std::collections::HashSet;
	use bitcoin::{OutPoint, Txid};
	use rand::RngCore;
	use std::thread;
	use bitcoin::consensus::Encodable;
	use rand::Rng;
	use self::rocksdb::IteratorMode;

	// just a small benchmark
	// #[test]
	#[allow(unused)]
	fn main1() {
		let len = 600000;
		let start = Instant::now();
		let txids: Vec<UtxoMerkleNode> = (0..len)
			.map(|x: u128| UtxoMerkleNode::from_slice(&x.to_be_bytes().repeat(2)).unwrap()).collect();
		let matches: Vec<bool> = (0..len).map(|_| false).collect();
		println!("prepared in {} ms", start.elapsed().as_millis());
		let start = Instant::now();
		let pmt = PartialMerkleTree::from_utxos(&txids, &matches);
		println!("built in {} ms", start.elapsed().as_millis());
		let root = pmt.extract_matches(&mut vec![], &mut vec![]).unwrap();
		println!("extract in {} ms", start.elapsed().as_millis());
		println!("{:?}", root);
	}

	fn utxos_in_mem() {
		let mut set = HashSet::new();
		let mut rng = rand::thread_rng();

		for i in 0..30_000_000 {
			let utxo = OutPoint::new(Txid::default(), rng.next_u32());
			set.insert(utxo);
		}
		println!("sleeping");
		thread::sleep(Duration::new(9999, 0));
	}

	fn utxos_in_store_rocksdb() {
		use rocksdb::{DB};
		let mut rng = rand::thread_rng();
		let db = DB::open_default("/tmp/r").unwrap();
		for i in 0..30_000_000 {
			let mut txid = [0u8; 32];
			rng.fill(&mut txid);
			let utxo = OutPoint::new(Txid::from_slice(&txid).unwrap(), rng.next_u32());
			let mut buf = Vec::new();
			utxo.consensus_encode(&mut buf).unwrap();
			db.put(buf, &[]).unwrap();
			if i % 100000 == 0 {
				println!("{}", i);
			}
		}
		println!("sleeping");
		thread::sleep(Duration::new(9999, 0));
	}

	fn iter_utxos() {
		use rocksdb::{DB};
		let db = DB::open_default("/tmp/r").unwrap();
		let iter = db.iterator(IteratorMode::Start);
		println!("counting");
		let mut count = 0;
		for (k, v) in iter {
			count += 1;
		}
		println!("counted {}", count)
	}
}
