use std::mem::replace;
use std::sync::Arc;

use bitcoin::BlockHash;

use crate::storage::Storage;
use crate::utxo::{UtxoListener, Utxos, UtxosDelta};

pub const DEFAULT_EPOCH_LENGTH: u32 = 16;

pub struct Builder {
	pub(crate) epoch_length: u32,
	// the height of the next block we are expecting
	pub(crate) next_height: u32,
	// the next epoch start
	pub(crate) next_epoch: u32,
	// incremental UTXO change from previous epoch start to now
	pub(crate) delta_to_previous_epoch: UtxosDelta,
	// incremental UTXO change from current epoch start to now
	pub(crate) delta_to_current_epoch: UtxosDelta,

	pub(crate) storage: Arc<dyn Storage>,
}

impl UtxoListener for Builder {
	fn connect(&mut self, block_hash: BlockHash, height: u32, adds: Utxos, removes: Utxos) {
		assert_eq!(height, self.next_height);
		// current epoch is [next_epoch - length, next_epoch)

		// We may have jumped back, either because we lost some blocks during a reorg,
		// or because we restarted and are re-accumulating the deltas.  Only accumulate into
		// a delta if we are in the right epoch.
		if height + self.epoch_length * 2 >= self.next_epoch {
			self.delta_to_current_epoch.connect(&adds, &removes);
		}
		if height + self.epoch_length >= self.next_epoch {
			self.delta_to_previous_epoch.connect(&adds, &removes);
		}

		// Refrain from notifying storage if we are re-accumulating on restart
		if height + self.epoch_length >= self.next_epoch {
			self.storage.connect_block(height, block_hash, &self.delta_to_previous_epoch);
		}
		self.next_height += 1;

		if self.next_height == self.next_epoch {
			self.storage.next_epoch(height, block_hash, &self.delta_to_current_epoch);
			self.next_epoch += self.epoch_length;
		}

		if self.next_height % self.epoch_length == 0 {
			// new epoch, shift deltas down to account for that
			self.delta_to_previous_epoch =
				replace(&mut self.delta_to_current_epoch, UtxosDelta::new());
		}
	}

	fn disconnect(&mut self, _block_hash: BlockHash, height: u32, adds: Utxos, removes: Utxos) {
		assert_eq!(height, self.next_height - 1);
		// if we reorged so that the height fell too far back, don't process
		// current epoch is [next_epoch - length, next_epoch)
		if height + self.epoch_length >= self.next_epoch {
			self.delta_to_current_epoch.disconnect(&adds, &removes);
		}
		if height + self.epoch_length * 2 >= self.next_epoch {
			self.delta_to_previous_epoch.disconnect(&adds, &removes);
		}
		self.next_height -= 1;

		todo!() // unfinished implementation
	}
}

impl Builder {
	pub fn new(epoch_length: u32, next_height: u32, storage: Arc<dyn Storage>) -> Self {
		// When we restart, we start one epoch behind the current epoch, and the next epoch
		// is one after the current epoch.
		// In [`Builder::connect`] we will accumulate into delta_to_current_epoch for an epoch,
		// while refraining from notifying the storage.  Then we will move this delta to
		// delta_to_previous_epoch and continue as normal.
		// If we start from scratch, we start accumulating and notifying from block zero.
		let next_epoch =
			if next_height == 0 { epoch_length }
			else { next_height + 2 * epoch_length };

		Builder {
			epoch_length,
			next_height,
			next_epoch,
			delta_to_previous_epoch: UtxosDelta::new(),
			delta_to_current_epoch: UtxosDelta::new(),
			storage
		}
	}
}

#[cfg(test)]
mod tests {
	use bitcoin::{OutPoint, Txid};

	use super::*;

	struct DummyStorage();

	impl Storage for DummyStorage {
		fn connect_block(&self, _new_height: u32, _block_hash: BlockHash, _delta: &UtxosDelta) {
		}

		fn next_epoch(&self, _new_height: u32, _epoch_end_height: BlockHash, _delta_to_current_epoch: &UtxosDelta) {
		}
	}

	fn make_storage() -> Arc<dyn Storage> {
		Arc::new(DummyStorage())
	}

	#[test]
	fn test_epochs() {
		let mut builder = Builder::new(3, 0, make_storage());
		let hash0 = BlockHash::default();
		let tx0 = Txid::default();

		// forward 6 blocks
		for i in 0..6 {
			let mut adds = Utxos::new();
			adds.push(OutPoint::new(tx0, i));
			builder.connect(hash0, i, adds, Utxos::new());
		}
		assert_eq!(builder.delta_to_current_epoch.adds.len(), 0);
		assert_eq!(builder.delta_to_previous_epoch.adds.len(), 3);

		// back 3 blocks
		for i in 0..3 {
			let mut adds = Utxos::new();
			adds.push(OutPoint::new(tx0, 5 - i));
			builder.disconnect(hash0, 5 - i, adds, Utxos::new());
		}
		assert_eq!(builder.delta_to_current_epoch.adds.len(), 0);
		assert_eq!(builder.delta_to_previous_epoch.adds.len(), 0);

		// back 3 blocks
		for i in 0..3 {
			let mut adds = Utxos::new();
			adds.push(OutPoint::new(tx0, 3 - i));
			builder.disconnect(hash0, 2 - i, adds, Utxos::new());
		}
		assert_eq!(builder.delta_to_current_epoch.adds.len(), 0);
		assert_eq!(builder.delta_to_previous_epoch.adds.len(), 0);

		// forward again, we get to the same place
		for i in 0..6 {
			let mut adds = Utxos::new();
			adds.push(OutPoint::new(tx0, i));
			builder.connect(hash0, i, adds, Utxos::new());
		}
		assert_eq!(builder.delta_to_current_epoch.adds.len(), 0);
		assert_eq!(builder.delta_to_previous_epoch.adds.len(), 3);
	}

	#[test]
	#[should_panic]
	fn test_unexpected_height() {
		let mut builder = Builder::new(3, 0, make_storage());
		let hash0 = BlockHash::default();
		builder.connect(hash0, 0, Utxos::new(), Utxos::new());
		builder.connect(hash0, 2, Utxos::new(), Utxos::new());
	}

	#[test]
	#[should_panic]
	fn test_unexpected_disconnect() {
		let mut builder = Builder::new(3, 0, make_storage());
		let hash0 = BlockHash::default();
		let tx0 = Txid::default();

		// forward 6 blocks
		for i in 0..6 {
			let mut adds = Utxos::new();
			adds.push(OutPoint::new(tx0, i));
			builder.connect(hash0, i, adds, Utxos::new());
		}

		let mut adds = Utxos::new();
		adds.push(OutPoint::new(tx0, 999));
		builder.disconnect(hash0, 5, adds, Utxos::new());
	}
}