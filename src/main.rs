extern crate bitcoin;
extern crate clap;
extern crate log;
extern crate serde;
extern crate serde_json;
extern crate url;
extern crate rocksdb;

use bitcoin::Network;
use clap::Clap;
use url::Url;

use crate::bitcoind_client::{BitcoindClient, BlockSource};
use crate::chainsync::{Syncer, ChainTip, ChainListener};
use crate::utxo::UtxoExtractor;
use crate::builder::{Builder, DEFAULT_EPOCH_LENGTH};
use crate::storage::UtxoSledStorage;
use std::sync::Arc;
use std::thread::sleep;
use std::time::Duration;

mod chainsync;
mod convert;
mod bitcoind_client;
mod tree;
mod utxo;
mod builder;
mod storage;

#[derive(Clap)]
#[clap(version = "0.1")]
struct Opts {
    /// Sets a custom config file. Could have been an Option<T> with no default too
    #[clap(short, long, default_value = "signet")]
    network: Network,
    #[clap(short, long, about = "bitcoind RPC URL, must have http(s) schema")]
    rpc: Option<String>,
    #[clap(long, about = "start computing commitments at height", default_value = "0")]
    fast_sync: u32,
    #[clap(long, about = "dev - stop at height")]
    stop_at: Option<u32>,
    #[clap(subcommand)]
    subcmd: Option<Subcommand>,
}

#[derive(Clap, Debug, Clone, Copy)]
enum Subcommand {
    Dump
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();

    let opts: Opts = Opts::parse();
    let network = opts.network.clone();
    let rpc_s = opts.rpc.clone().unwrap_or_else(||
        match network {
            Network::Bitcoin => "http://user:pass@localhost:8332",
            Network::Testnet => "http://user:pass@localhost:18332",
            Network::Signet => "http://user:pass@localhost:38442",
            Network::Regtest => "http://user:pass@localhost:18332",
        }.to_owned()
    );

    let rpc = Url::parse(&rpc_s).expect("rpc url");

    match opts.subcmd {
        None => {
            run_daemon(opts, network, rpc).await?;
        }
        Some(Subcommand::Dump) => {
            dump(opts, network).await
        }
    }

    Ok(())
}

async fn dump(_opts: Opts, network: Network) {
    let epoch_length = DEFAULT_EPOCH_LENGTH;
    let storage = UtxoSledStorage::new(network, epoch_length, 0).await;
    storage.dump();
}

async fn run_daemon(opts: Opts, network: Network, rpc: Url) -> anyhow::Result<()> {
    println!("bitcoind RPC {}", rpc);

    let client = BitcoindClient::new(rpc.host_str().expect("rpc host").to_owned(),
                                     rpc.port().expect("rpc port"),
                                     rpc.username().to_owned(),
                                     rpc.password().to_owned().expect("rpc password").to_owned()).await?;
    let info = client.get_blockchain_info().await;
    println!("{:?}", info);
    let epoch_length = DEFAULT_EPOCH_LENGTH;
    let storage = Arc::new(UtxoSledStorage::new(network, epoch_length, opts.fast_sync).await);
    let current_epoch_height_opt = storage.current_epoch_height();
    let current_tip = if let Some(current_epoch_height) = current_epoch_height_opt {
        // restart from the previous epoch end, to rebuild the UTXO delta from that point
        if current_epoch_height >= epoch_length {
            let height = current_epoch_height - epoch_length;
            // we don't handle the block disappearing from the block source
            let block_hash = client.get_block_hash(height).await.unwrap().unwrap();
            Some(ChainTip(height, block_hash))
        } else {
            // if we have done exactly one epoch, we should start from scratch, so we include genesis in the delta
            None
        }
    } else {
        None
    };

    let start_epoch_height = current_tip.map(|t| t.0 + 1).unwrap_or(0);
    log::info!("restarting at epoch_height {}", start_epoch_height);
    let builder = Builder::new(epoch_length, start_epoch_height, storage);
    let mut syncer = Syncer::new(client);
    let mut extractor = UtxoExtractor::new(builder, current_tip);
    let mut caught_up: bool = false;
    loop {
        if syncer.sync(&mut extractor).await {
            caught_up = false;
        } else {
            if !caught_up {
                log::info!("caught up, waiting for more blocks");
                caught_up = true;
            }
            sleep(Duration::new(1, 0));
        }
        if let Some(stop_at) = opts.stop_at {
            if extractor.current_tip().unwrap().0 == stop_at {
                break;
            }
        }
    }

    Ok(())
}
