use bitcoin::{Block, BlockHash};

use crate::bitcoind_client::BlockSource;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::task::JoinHandle;

#[derive(Debug, Copy, Clone)]
pub struct ChainTip(pub u32, pub BlockHash);

pub trait ChainListener {
	fn current_tip(&self) -> Option<ChainTip>;
	fn connect_block(&mut self, block: Block);
	fn disconnect_block(&mut self, block: Block);
}

pub struct Syncer<S: BlockSource> {
	source: Arc<S>,
	pending_gets: HashMap<u32, JoinHandle<Option<Block>>>
}

impl<S: 'static + BlockSource> Syncer<S> {
	pub fn new(source: S) -> Self {
		Syncer { source: Arc::new(source), pending_gets: HashMap::new() }
	}

	async fn get_block_at_height(&mut self, height: u32) -> Option<Block> {
		if let Some(f) = self.pending_gets.remove(&height) {
			return f.await.unwrap();
		}
		for i in 1..10 {
			if !self.pending_gets.contains_key(&(height + i)) {
				let source = Arc::clone(&self.source);
				let task = async move {
					Self::do_get_block_at_height(source, height + i).await
				};
				let handle = tokio::spawn(task);
				self.pending_gets.insert(height + i, handle);
			}
		}
		Self::do_get_block_at_height(Arc::clone(&self.source), height).await
	}

	async fn do_get_block_at_height(source: Arc<S>, height: u32) -> Option<Block> {
		let res = match source.get_block_hash(height).await.unwrap() {
			None => None,
			Some(hash) => Some(source.get_block(&hash)
				.await.unwrap())
		};
		res
	}

	/// Perform one chain sync step
	pub async fn sync<T: ChainListener>(&mut self, listener: &mut T) -> bool {
		let current = listener.current_tip();
		match current {
			None => {
				let genesis_hash = self.source.get_block_hash(0)
					.await.unwrap().expect("must have a genesis block");
				let genesis_block = self.source.get_block(&genesis_hash)
					.await.unwrap();
				listener.connect_block(genesis_block);
				true
			}
			Some(tip) => {
				let block = self.get_block_at_height(tip.0 + 1).await;

				match block {
					None => {
						let (chain_tip_hash, chain_tip_height) = self.source.get_best_block()
							.await.unwrap();
						if chain_tip_hash != tip.1 {
							// Due to a race, we might not have gotten anything new when we called
							// `get_block_hash` above, but did get a new block when we called `get_best_block`.
							// Therefore, check the height of the tip before deciding there was a reorg at the tip.
							// If we actually got a new block or lost some blocks, we'll see those
							// on the next call to this function.
							if chain_tip_height == tip.0 {
								log::info!("reorg at tip {}: {} -> {}", tip.0, tip.1, chain_tip_hash);
								let old_block = self.source.get_block(&tip.1)
									.await.unwrap();
								listener.disconnect_block(old_block);
							}
							true
						} else {
							log::debug!("no change at {}", tip.0);
							false
						}
					}
					Some(new_block) => {
						let prev_blockhash = new_block.header.prev_blockhash;
						if prev_blockhash != tip.1 {
							log::info!("reorg while catching up at {}: {} -> {}", tip.0, tip.1, prev_blockhash);
							let old_block = self.source.get_block(&tip.1)
								.await.unwrap();
							listener.disconnect_block(old_block);
						} else {
							log::debug!("new block at {}: {}", tip.0 + 1, new_block.header.block_hash());
							listener.connect_block(new_block);
						}
						true
					}
				}
			}
		}
	}
}