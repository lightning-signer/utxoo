use bitcoin::{Block, OutPoint, BlockHash};

use crate::chainsync::{ChainListener, ChainTip};
use std::ops::{Deref, DerefMut};
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct Utxos(Vec<OutPoint>);

impl Utxos {
	pub fn new() -> Self {
		Utxos(Vec::new())
	}
}

impl Deref for Utxos {
	type Target = Vec<OutPoint>;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Utxos {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

pub trait UtxoListener {
	fn connect(&mut self, block_hash: BlockHash, height: u32, adds: Utxos, removes: Utxos);
	fn disconnect(&mut self, block_hash: BlockHash, height: u32, adds: Utxos, removes: Utxos);
}

pub struct UtxoExtractor<L: UtxoListener> {
	tip: Option<ChainTip>,
	total_txs: u64,
	listener: L,
}

impl<L: UtxoListener> UtxoExtractor<L> {
	pub(crate) fn new(listener: L, tip: Option<ChainTip>) -> Self {
		UtxoExtractor {
			tip,
			total_txs: 0,
			listener
		}
	}
}

impl<L: UtxoListener> ChainListener for UtxoExtractor<L> {
	fn current_tip(&self) -> Option<ChainTip> {
		self.tip
	}

	fn connect_block(&mut self, block: Block) {
		let block_hash = block.block_hash();
		self.total_txs += block.txdata.len() as u64;
		let new_height = self.tip.map(|t| t.0 + 1).unwrap_or(0);
		let (adds, removes) = Self::block_to_utxos(block);
		log::info!("connect block {} at {} +{} -{} seen {} txs", block_hash, new_height, adds.len(), removes.len(), self.total_txs);
		self.tip = Some(ChainTip(new_height, block_hash));
		self.listener.connect(block_hash, new_height, adds, removes);
	}

	fn disconnect_block(&mut self, block: Block) {
		log::info!("disconnect block at {:?}", self.tip);
		let prev_hash = block.header.prev_blockhash;
		let block_txs = block.txdata.len();

		let ChainTip(cur_height, cur_hash) =
			self.tip.expect("disconnect at genesis");
		assert_eq!(cur_hash, block.block_hash());

		let (adds, removes) = Self::block_to_utxos(block);
		self.listener.disconnect(cur_hash, cur_height, adds, removes);

		self.total_txs -= block_txs as u64;
		if cur_height == 0 {
			self.tip = None
		} else {
			self.tip = Some(ChainTip(cur_height - 1, prev_hash))
		}
	}
}

impl<L: UtxoListener> UtxoExtractor<L> {
	fn block_to_utxos(block: Block) -> (Utxos, Utxos) {
		let mut adds = Utxos::new();
		let mut removes = Utxos::new();

		for tx in block.txdata {
			let txid = tx.txid();
			for inp in tx.input {
				removes.push(inp.previous_output)
			}
			for (i, _) in tx.output.iter().enumerate() {
				adds.push(OutPoint::new(txid, i as u32))
			}
		}

		(adds, removes)
	}
}

/// A delta describing the additional UTXOs created and destroyed since a certain block
#[derive(Debug, Clone)]
pub struct UtxosDelta {
	pub adds: Utxos,
	pub removes: Utxos,
}

impl UtxosDelta {
	pub fn new() -> Self {
		UtxosDelta {
			adds: Utxos::new(),
			removes: Utxos::new()
		}
	}

	pub fn connect(&mut self, adds: &Utxos, removes: &Utxos) {
		self.adds.extend(adds.iter());
		self.removes.extend(removes.iter());
	}

	pub fn disconnect(&mut self, adds: &Utxos, removes: &Utxos) {
		assert!(self.adds.ends_with(adds));
		assert!(self.removes.ends_with(removes));
		let new_adds_len = self.adds.len() - adds.len();
		self.adds.truncate(new_adds_len);
		let new_removes_len = self.removes.len() - removes.len();
		self.removes.truncate(new_removes_len);
	}

	pub fn minimize(&self) -> UtxosDelta {
		let adds: HashSet<_> = self.adds.iter().cloned().collect();
		let removes: HashSet<_> = self.removes.iter().cloned().collect();
		UtxosDelta {
			adds: Utxos(adds.difference( &removes).cloned().collect()),
			removes: Utxos(removes.difference( &adds).cloned().collect()),
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::utxo::{UtxosDelta, Utxos};
	use bitcoin::{OutPoint, Txid};

	#[test]
	fn test_minimize_empty() {
		let mut delta = UtxosDelta::new();
		delta.connect(&Utxos(vec![]), &Utxos(vec![]));
		let min_delta = delta.minimize();
		assert_eq!(*min_delta.adds, *Utxos(vec![]));
		assert_eq!(*min_delta.removes, *Utxos(vec![]));
	}


	#[test]
	fn test_minimize() {
		let mut delta = UtxosDelta::new();
		delta.connect(
			&Utxos(vec![make_outpoint(0), make_outpoint(1), make_outpoint(2)]),
			&Utxos(vec![make_outpoint(1), make_outpoint(3)])
		);
		let min_delta = delta.minimize();
		let mut sorted_adds = (*min_delta.adds).clone();
		sorted_adds.sort();
		assert_eq!(sorted_adds, *Utxos(vec![make_outpoint(0), make_outpoint(2)]));
		assert_eq!(*min_delta.removes, *Utxos(vec![make_outpoint(3)]));
	}

	fn make_outpoint(i: u32) -> OutPoint {
		OutPoint::new(Txid::default(), i)
	}
}