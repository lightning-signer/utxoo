use bitcoin::consensus::Encodable;
use bitcoin::hashes::{borrow_slice_impl, Hash, hash_newtype, hex_fmt_impl, index_impl, serde_impl, sha256d, HashEngine};

use crate::tree::MerkleError::{BadFormat, NoItems};

hash_newtype!(UtxoMerkleNode, sha256d::Hash, 32, doc="A hash of the Merkle tree branch or root for utxos");

macro_rules! impl_hashencode {
    ($hashtype:ident) => {
        impl bitcoin::consensus::Encodable for $hashtype {
            fn consensus_encode<S: ::std::io::Write>(&self, s: S) -> Result<usize, ::std::io::Error> {
                self.0.consensus_encode(s)
            }
        }

        impl bitcoin::consensus::Decodable for $hashtype {
            fn consensus_decode<D: ::std::io::Read>(d: D) -> Result<Self, bitcoin::consensus::encode::Error> {
                use bitcoin::hashes::Hash;
                Ok(Self::from_inner(<<$hashtype as bitcoin::hashes::Hash>::Inner>::consensus_decode(d)?))
            }
        }
    }
}

impl_hashencode!(UtxoMerkleNode);

/// An error when verifying the merkle block
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum MerkleError {
	/// When partial merkle tree contains no items
	NoItems,
	/// General format error
	BadFormat(String),
}

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct PartialMerkleTree {
	/// The total number of UTXOs
	size: u32,
	/// node-is-parent-of-matched-utxo bits
	bits: Vec<bool>,
	/// Transaction ids and internal hashes
	hashes: Vec<UtxoMerkleNode>,
}

impl PartialMerkleTree {
	pub fn from_utxos(utxos: &[UtxoMerkleNode], matches: &[bool]) -> Self {
		assert_eq!(utxos.len(), matches.len());

		let mut pmt = PartialMerkleTree {
			size: utxos.len() as u32,
			bits: Vec::with_capacity(utxos.len()),
			hashes: vec![],
		};
		// calculate height of tree
		let mut height = 0;
		while pmt.calc_tree_width(height) > 1 {
			height += 1;
		}
		// traverse the partial tree
		pmt.traverse_and_build(height, 0, utxos, matches);
		pmt
	}

	/// Extract the matching utxo's represented by this partial merkle tree
	/// and their respective indices within the partial tree.
	/// returns the merkle root, or error in case of failure
	pub fn extract_matches(
		&self,
		matches: &mut Vec<UtxoMerkleNode>,
		indexes: &mut Vec<u32>,
	) -> Result<UtxoMerkleNode, MerkleError> {
		matches.clear();
		indexes.clear();
		// An empty set will not work
		if self.size == 0 {
			return Err(NoItems);
		};
		// there can never be more hashes provided than one for every utxo
		if self.hashes.len() as u32 > self.size {
			return Err(BadFormat(
				"Proof contains more hashes than transactions".to_owned(),
			));
		};
		// there must be at least one bit per node in the partial tree, and at least one node per hash
		if self.bits.len() < self.hashes.len() {
			return Err(BadFormat("Proof contains less bits than hashes".to_owned()));
		};
		// calculate height of tree
		let mut height = 0;
		while self.calc_tree_width(height) > 1 {
			height += 1;
		}
		// traverse the partial tree
		let mut bits_used = 0u32;
		let mut hash_used = 0u32;
		let hash_merkle_root =
			self.traverse_and_extract(height, 0, &mut bits_used, &mut hash_used, matches, indexes)?;
		// Verify that all bits were consumed (except for the padding caused by
		// serializing it as a byte sequence)
		if (bits_used + 7) / 8 != (self.bits.len() as u32 + 7) / 8 {
			return Err(BadFormat("Not all bit were consumed".to_owned()));
		}
		// Verify that all hashes were consumed
		if hash_used != self.hashes.len() as u32 {
			return Err(BadFormat("Not all hashes were consumed".to_owned()));
		}
		Ok(UtxoMerkleNode::from_inner(hash_merkle_root.into_inner()))
	}

	/// Helper function to efficiently calculate the number of nodes at given height
	/// in the merkle tree
	#[inline]
	fn calc_tree_width(&self, height: u32) -> u32 {
		(self.size + (1 << height) - 1) >> height
	}

	/// Calculate the hash of a node in the merkle tree (at leaf level: the utxo's themselves)
	fn calc_hash(&self, height: u32, pos: u32, utxos: &[UtxoMerkleNode]) -> UtxoMerkleNode {
		if height == 0 {
			// Hash at height 0 is the utxo itself
			UtxoMerkleNode::from_inner(utxos[pos as usize].into_inner())
		} else {
			// Calculate left hash
			let left = self.calc_hash(height - 1, pos * 2, utxos);
			// Calculate right hash if not beyond the end of the array - copy left hash otherwise
			let right = if pos * 2 + 1 < self.calc_tree_width(height - 1) {
				self.calc_hash(height - 1, pos * 2 + 1, utxos)
			} else {
				left
			};
			// Combine subhashes
			parent_hash(left, right)
		}
	}

	/// Recursive function that traverses tree nodes, storing the data as bits and hashes
	fn traverse_and_build(
		&mut self,
		height: u32,
		pos: u32,
		utxos: &[UtxoMerkleNode],
		matches: &[bool],
	) {
		// Determine whether this node is the parent of at least one matched utxo
		let mut parent_of_match = false;
		let mut p = pos << height;
		while p < (pos + 1) << height && p < self.size {
			parent_of_match |= matches[p as usize];
			p += 1;
		}
		// Store as flag bit
		self.bits.push(parent_of_match);

		if height == 0 || !parent_of_match {
			// If at height 0, or nothing interesting below, store hash and stop
			let hash = self.calc_hash(height, pos, utxos);
			self.hashes.push(hash);
		} else {
			// Otherwise, don't store any hash, but descend into the subtrees
			self.traverse_and_build(height - 1, pos * 2, utxos, matches);
			if pos * 2 + 1 < self.calc_tree_width(height - 1) {
				self.traverse_and_build(height - 1, pos * 2 + 1, utxos, matches);
			}
		}
	}

	/// Recursive function that traverses tree nodes, consuming the bits and hashes produced by
	/// TraverseAndBuild. It returns the hash of the respective node and its respective index.
	fn traverse_and_extract(
		&self,
		height: u32,
		pos: u32,
		bits_used: &mut u32,
		hash_used: &mut u32,
		matches: &mut Vec<UtxoMerkleNode>,
		indexes: &mut Vec<u32>,
	) -> Result<UtxoMerkleNode, MerkleError> {
		if *bits_used as usize >= self.bits.len() {
			return Err(BadFormat("Overflowed the bits array".to_owned()));
		}
		let parent_of_match = self.bits[*bits_used as usize];
		*bits_used += 1;
		if height == 0 || !parent_of_match {
			// If at height 0, or nothing interesting below, use stored hash and do not descend
			if *hash_used as usize >= self.hashes.len() {
				return Err(BadFormat("Overflowed the hash array".to_owned()));
			}
			let hash = self.hashes[*hash_used as usize];
			*hash_used += 1;
			if height == 0 && parent_of_match {
				// in case of height 0, we have a matched utxo
				matches.push(UtxoMerkleNode::from_inner(hash.into_inner()));
				indexes.push(pos);
			}
			Ok(hash)
		} else {
			// otherwise, descend into the subtrees to extract matched utxos and hashes
			let left = self.traverse_and_extract(
				height - 1,
				pos * 2,
				bits_used,
				hash_used,
				matches,
				indexes,
			)?;
			let right;
			if pos * 2 + 1 < self.calc_tree_width(height - 1) {
				right = self.traverse_and_extract(
					height - 1,
					pos * 2 + 1,
					bits_used,
					hash_used,
					matches,
					indexes,
				)?;
				if right == left {
					// The left and right branches should never be identical, as the transaction
					// hashes covered by them must each be unique.
					return Err(BadFormat("Found identical transaction hashes".to_owned()));
				}
			} else {
				right = left;
			}
			// and combine them before returning
			Ok(parent_hash(left, right))
		}
	}
}

/// Helper method to produce SHA256D(left + right)
fn parent_hash(left: UtxoMerkleNode, right: UtxoMerkleNode) -> UtxoMerkleNode {
	let mut encoder = UtxoMerkleNode::engine();
	left.consensus_encode(&mut encoder).unwrap();
	right.consensus_encode(&mut encoder).unwrap();
	UtxoMerkleNode::from_engine(encoder)
}

fn merkle_root<I: Iterator<Item = UtxoMerkleNode>>(utxos: I, min_height: Option<usize>) -> UtxoMerkleNode {
	let mut state: Vec<Option<UtxoMerkleNode>> = vec![];
	if let Some(min_height) = min_height {
		state.resize(min_height, None);
	}

	for mut new_item in utxos {
		for i in 0..state.len() + 1 {
			if i == state.len() {
				state.push(Some(new_item));
			} else if let Some(left) = state[i] {
				new_item = parent_hash(left, new_item);
				state[i] = None
			} else {
				state[i] = Some(new_item);
				break;
			}
		}
		// println!("{:?}", state);
	}

	if state.is_empty() {
		return UtxoMerkleNode::default();
	}
	let mut new_item_o = None;
	for i in 0..state.len() - 1 {
		if let Some(left) = state[i] {
			new_item_o = Some(parent_hash(left, new_item_o.unwrap_or(left)))
		} else {
			new_item_o = new_item_o.map(|i| parent_hash(i, i))
		}
		// println!("{:?}", new_item_o);
	}

	let top_o = state[state.len() - 1];
	if let Some(new_item) = new_item_o {
		if let Some(top) = top_o {
			parent_hash(top, new_item)
		} else {
			new_item
		}
	} else {
		top_o.unwrap()
	}
}

use itertools::Itertools;

// Compute the first merkle tree level above the leafs
fn merkle_level_0<I: Iterator<Item=Box<[u8]>>>(iter: I) -> Vec<UtxoMerkleNode> {
	let mut tuples = iter.tuples();
	let mut level1 = Vec::new();

	loop {
		if let Some((left, right)) = tuples.next() {
			let mut encoder = UtxoMerkleNode::engine();
			encoder.input(&left);
			encoder.input(&right);
			level1.push(UtxoMerkleNode::from_engine(encoder))
		} else {
			break
		}
	}
	if let Some(leftover) = tuples.into_buffer().next() {
		let mut encoder = UtxoMerkleNode::engine();
		encoder.input(&leftover);
		encoder.input(&leftover);
		level1.push(UtxoMerkleNode::from_engine(encoder))
	}

	level1
}

pub fn merkle_root_from_level0<I: Iterator<Item = Box<[u8]>>>(utxos: I) -> UtxoMerkleNode {
	let level1 = merkle_level_0(utxos);
	merkle_root(level1.into_iter(), None)
}

pub async fn merkle_root_from_level0_async<I: Iterator<Item = Box<[u8]>>>(chunk_exp: usize, mut utxos: I) -> UtxoMerkleNode {
	let mut handles = Vec::new();
	let chunk_size = 1 << chunk_exp;
	loop {
		let mut chunk = Vec::with_capacity(chunk_size);
		for _ in 0..chunk_size {
			if let Some(item) = utxos.next() {
				chunk.push(item);
			}
		}
		let len = chunk.len();
		if len == 0 {
			break;
		}
		if handles.is_empty() && chunk.len() < chunk_size {
			// Less than one chunk is not handled below, because we are forcing a minimum height, so use the non-async version
			return merkle_root_from_level0(chunk.into_iter());
		}
		handles.push(tokio::spawn(
			async move {
				let level1 = merkle_level_0(chunk.into_iter());
				// min height should be log2(chunk_size) + 1, but we already did a level in the previous line
				merkle_root(level1.into_iter(), Some(chunk_exp))
			}
		));
		if len < chunk_size {
			break;
		}
	}
	let mut midlevel = Vec::with_capacity(handles.len());
	for handle in handles {
		midlevel.push(handle.await.unwrap())
	}
	// println!("MIDLEVEL {:?}", midlevel);
	let res = merkle_root(midlevel.into_iter(), None);
	// println!("DONE");
	res
}

#[cfg(test)]
mod tests {
	use crate::tree::{UtxoMerkleNode, PartialMerkleTree, merkle_root, merkle_root_from_level0, merkle_root_from_level0_async};
	use bitcoin::hashes::Hash;

	#[test]
	fn check() {
		for i in 1u32..31 {
			do_check(i);
		}
	}

	fn do_check(i: u32) {
		let items: Vec<_> = make_items(i).iter().map(|s|
			UtxoMerkleNode::from_slice(&s).unwrap()).collect();
		let matches: Vec<bool> = items.iter().map(|_| false).collect();
		let pmt = PartialMerkleTree::from_utxos(items.as_slice(), &matches);
		let root = pmt.extract_matches(&mut vec![], &mut vec![]).unwrap();
		let root1 = merkle_root(items.into_iter(), None);
		println!("{:?}", root);
		assert_eq!(root, root1);
	}

	#[tokio::test]
	async fn check_async() {
		for i in 1u32..31 {
			do_check_async(i).await;
		}
	}

	async fn do_check_async(i: u32) {
		let items = make_items(i);
		let res_async = merkle_root_from_level0_async(3, items.clone().into_iter()).await;
		let res = merkle_root_from_level0(items.clone().into_iter());
		assert_eq!(res, res_async, "mismatch with i={}", i);
	}

	fn make_items(len: u32) -> Vec<Box<[u8]>> {
		(0..len).map(|i| {
			let mut slice = [0x11u8; 32];
			slice[0..4].copy_from_slice(&i.to_be_bytes());
			Box::new(slice) as Box<[u8]>
		}).collect()
	}
}